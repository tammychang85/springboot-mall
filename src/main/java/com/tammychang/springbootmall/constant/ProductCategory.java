package com.tammychang.springbootmall.constant;

public enum ProductCategory {
    FOOD,
    CAR,
    BOOK
}
