package com.tammychang.springbootmall.service;

import com.tammychang.springbootmall.dto.ProductQueryParams;
import com.tammychang.springbootmall.dto.ProductRequest;
import com.tammychang.springbootmall.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> getProducts(ProductQueryParams productQueryParams);

    Product getProductById(Integer productId);

    Integer createProduct(ProductRequest productRequest);

    void updateProduct(Integer productId, ProductRequest productRequest);

    void deleteProduct(Integer productId);

    Integer countProduct(ProductQueryParams productQueryParams);
}
