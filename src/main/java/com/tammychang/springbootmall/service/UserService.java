package com.tammychang.springbootmall.service;

import com.tammychang.springbootmall.dto.UserLoginRequest;
import com.tammychang.springbootmall.dto.UserRegisterRequest;
import com.tammychang.springbootmall.model.User;

public interface UserService {

    Integer register(UserRegisterRequest userRegisterRequest);

    User getUserById(Integer userId);

    User login(UserLoginRequest userLoginRequest);

}
