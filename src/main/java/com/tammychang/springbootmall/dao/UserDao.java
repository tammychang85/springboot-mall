package com.tammychang.springbootmall.dao;

import com.tammychang.springbootmall.dto.UserRegisterRequest;
import com.tammychang.springbootmall.model.User;

public interface UserDao {

    Integer createUser(UserRegisterRequest userRegisterRequest);

    User getUserById(Integer userId);

    User getUserByEmail(String  email);

}
